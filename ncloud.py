import json
import ssl
import time
from nostr.filter import Filter, Filters
from nostr.event import Event, EventKind
from nostr.relay_manager import RelayManager
from nostr.message_type import ClientMessageType
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt

filters = Filters([Filter(kinds=[EventKind.TEXT_NOTE])])
subscription_id = "nostrcloud"
request = [ClientMessageType.REQUEST, subscription_id]
request.extend(filters.to_json_array())

relay_manager = RelayManager()
relay_manager.add_relay("wss://relay.primal.net")
relay_manager.add_subscription(subscription_id, filters)
relay_manager.open_connections({"cert_reqs": ssl.CERT_NONE}) 
time.sleep(1.25) 

message = json.dumps(request)
relay_manager.publish_message(message)
time.sleep(1)

notes_list = []

while relay_manager.message_pool.has_events():
    event_msg = relay_manager.message_pool.get_event()
    notes_list.append(event_msg.event.content)

relay_manager.close_connections()

num_last_notes = 500 
notes = " ".join(notes_list[-num_last_notes:])

stopwords = set(STOPWORDS)
wc = WordCloud(background_color="white", max_words=2000,
               stopwords=stopwords, mode="RGBA", colormap='BuPu', prefer_horizontal=1.0)
wc.generate(notes)

plt.figure(figsize=(10, 10))
plt.imshow(wc, interpolation='bilinear')
plt.axis("off")
plt.show()
